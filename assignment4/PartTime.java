package assignment4;

/**
 * This class is for Assignment 4 for COSC 351.101, Fall 2018.
 * @author Giovanni Vincenti, Halvor Remole, Nicholas Shrieves
 * @since 2018-10-12
 */

public class PartTime extends Faculty {
	
	int currentSlot = 0;
	
	/**
	 * Constructor that sets the default values.
	 */
	public PartTime() {
		super();
		coursesTaught[currentSlot] = 0;
		
		//this.currentSlot = currentSlot;
	}
	
	private int coursesTaught[] = new int[5];
	
	/**
	 * Sets the currentSlot
	 */
	public void setCurrentSlot(int currentSlot) {
		this.currentSlot = currentSlot;
	}

	/**
	 * Gets the number of courses taught.
	 * @return int containing the number of courses taught
	 */
	public int getCoursesTaught() {
		return coursesTaught[currentSlot];
	}

	/**
	 * Sets the number of courses taught.
	 * @param coursesTaught Number of courses taught (1-100)
	 * @return Boolean value reporting whether or not the attribute was successfully updated 
	 */
	public boolean setCoursesTaught(int coursesTaught) {
		if ( coursesTaught < 1 || coursesTaught > 100 )
			return false;
		
		this.coursesTaught[currentSlot] = coursesTaught;
		return true;
	}
	
	/**
	 * Method that returns in String format the information stored in the object.
	 * @return A string containing the information stored in the object
	 */
	public String toString() {
		String newline = System.getProperty("line.separator");
		String toReturn = super.toString();
		toReturn += "Number of courses taught: " + coursesTaught[currentSlot] + newline;
		return toReturn;
	}
}
