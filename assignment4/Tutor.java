package assignment4;

/**
 * This class is for Assignment 4 for COSC 351.101, Fall 2018.
 * @author Giovanni Vincenti, Halvor Remole, Nicholas Shrieves
 * @since 2018-10-12
 */

public class Tutor extends Student implements Employee {
	
	int currentSlot = 0;
	
	/**
	 * Constructor that sets the default values.
	 */
	public Tutor() {
		super();
		topics[currentSlot] = Person.NOT_AVAIL;
		hours[currentSlot] = 0;
		
		salary[currentSlot] = Employee.DEFAULT_SALARY;
		startDate[currentSlot] = Employee.DEFAULT_START_DATE;
		
		//this.currentSlot = currentSlot;
	}
	
	private String[] topics = new String[5];
	private int[] hours = new int[5];
	
	/**
	 * Sets the currentSlot
	 */
	public void setCurrentSlot(int currentSlot) {
		this.currentSlot = currentSlot;
	}
	
	/**
	 * Gets the list of topics that the tutor can teach.
	 * @return String containing the list of topics that the tutor can teach
	 */
	public String getTopics() {
		return topics[currentSlot];
	}
	
	/**
	 * Sets the topics that the tutor can teach
	 * @param topics List of topics in String format
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setTopics(String topics) {
		if ( topics == null )
			return false;
		
		topics = topics.trim();
		
		if ( !topics.matches("[a-zA-Z,\\ ]+") ) return false;
		
		this.topics[currentSlot] = topics;
		return true;
	}
	
	/**
	 * Gets the number of hours that the tutor has completed.
	 * @return int containing the number of hours that the tutor has completed
	 */
	public int getHours() {
		return hours[currentSlot];
	}
	
	/**
	 * Sets the number of hours that a tutor has already completed.
	 * @param hours Number of hours that a tutor has already completed
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setHours(int hours) {
		if ( hours < 0 || hours > 1000 )
			return false;
		
		this.hours[currentSlot] = hours;
		return true;
	}
	
	/**
	 * Method that returns in String format the information stored in the object.
	 * @return A string containing the information stored in the object
	 */
	public String toString() {
		String newline = System.getProperty("line.separator");
		String toReturn = super.toString();
		toReturn += "Employment start date: " + startDate[currentSlot] + newline;
		toReturn += "Salary: $" + salary[currentSlot] + newline;
		toReturn += "Topics: " + getTopics() + newline;
		toReturn += "Hours tutored: " + getHours() + newline;
		return toReturn;
	}
	
	private int salary[] = new int[5];
	private String startDate[] = new String[5];

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSalary() {
		return salary[currentSlot];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setSalary(int salary) {
		if ( salary < 0 )
			return false;
		this.salary[currentSlot] = salary;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStartDate() {
		return startDate[currentSlot];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setStartDate(String startDate) {
		if ( startDate == null ) return false;
		
		if ( !startDate.matches("[0-9]{4}/[0-9]{2}/[0-9]{2}") ) return false;
		
		this.startDate[currentSlot] = startDate;
		return true;
	}
}