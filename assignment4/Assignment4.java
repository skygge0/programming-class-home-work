package assignment4;

import java.io.IOException;
import java.util.Scanner;

/**
 * This class is for Assignment 4 for COSC 351.101, Fall 2018.
 * @author Giovanni Vincenti, Halvor Remole, Nicholas Shrieves
 * @since 2018-10-12
 */

public class Assignment4 {
	
	//static constructor calls so they can be access elsewhere
	static Person p = new Person();
	static Staff s = new Staff();
	static Faculty f = new Faculty();
	static FullTime ft = new FullTime();
	static PartTime pt = new PartTime();
	static Student st = new Student();
	static Tutor t = new Tutor();
	
	//static scanner to avoid creating several
	static Scanner userInput = new Scanner(System.in);
	
	//static variables used in several areas
	static int currentSlot = 0;
	static int totalSlots = 0;
	static int choiceWhat = 0;
	static int choiceObject = 0;
	
	/**
	 * Main method of the application.
	 * @param args Arguments from the command prompt - none are expected
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		
		while(true) {
			//top menu
			System.out.println("Choose what you would like to do.");
			System.out.println("1) Create a new record");
			System.out.println("2) View an existing record");
			System.out.println("3) Edit an existing record");
			
			//catches bad user input
			try {
			System.out.print("Choice: ");
			int choiceWhat= Integer.parseInt(userInput.nextLine());
			
			//create, view, edit menu
			switch(choiceWhat) {
			case 1: // Create
				if(5 <= totalSlots) {
					System.out.println("Too many records!");
					break;
					}
				currentSlot = totalSlots++;
				System.out.println("Choose which type of object you would like to create.");
				editItem();
				break;
			case 2: //View
				if(0 == totalSlots) {
					System.out.println("No records!");
					System.out.println();
					break;
					}
				viewItem();
				break;
			case 3: //Edit
				if(0 == totalSlots) {
					System.out.println("No records!");
					System.out.println();
					break;
					}
				System.out.println("Which record do you want to edit? (1-" + totalSlots + ")");
				currentSlot = Integer.parseInt((userInput.nextLine()));
				System.out.println("Choose which type of object you would like to edit.");
				editItem();
				break;
			default:
				System.out.println("Invalid choice");
				
			}
		
		//userInput.close();
		}catch(Exception e) {
			System.out.println("Bad input");
		}
		}
	}
		
	
	//creates an object record with data of user choosing
	public static void editItem(){
		
		// We will ask the user to select only one of the object types available
		
		System.out.println("1) Person");
		System.out.println("2) Staff");
		System.out.println("3) Faculty");
		System.out.println("4) Full-Time Faculty");
		System.out.println("5) Part-Time Faculty");
		System.out.println("6) Student");
		System.out.println("7) Tutor (Student)");
		
		System.out.print("Choice: ");
		int choiceObject = Integer.parseInt(userInput.nextLine());
		
		System.out.println();
		
		switch(choiceObject) {
		case 1: // Person
			p.setCurrentSlot(currentSlot);
			getPersonInfo(p, userInput);
			System.out.println("--- --- --- --- ---"); // Adding some separation
			System.out.println(p);
			break;
		case 2: // Staff
			s.setCurrentSlot(currentSlot);
			getStaffInfo(s, userInput);
			System.out.println("--- --- --- --- ---"); // Adding some separation
			System.out.println(s);
			break;
		case 3: // Faculty
			f.setCurrentSlot(currentSlot);
			getFacultyInfo(f, userInput);
			System.out.println("--- --- --- --- ---"); // Adding some separation
			System.out.println(f);
			break;
		case 4: //Full-Time Faculty
			ft.setCurrentSlot(currentSlot);
			getFullTimeFacultyInfo(ft, userInput);
			System.out.println("--- --- --- --- ---"); // Adding some separation
			System.out.println(ft);
			break;
		case 5: // Part-Time Faculty
			pt.setCurrentSlot(currentSlot);
			getPartTimeFacultyInfo(pt, userInput);
			System.out.println("--- --- --- --- ---"); // Adding some separation
			System.out.println(pt);
			break;
		case 6: // Student
			st.setCurrentSlot(currentSlot);
			getStudentInfo(st, userInput);
			System.out.println("--- --- --- --- ---"); // Adding some separation
			System.out.println(st);
			break;
		case 7: // Tutor
			t.setCurrentSlot(currentSlot);
			getTutorInfo(t, userInput);
			System.out.println("--- --- --- --- ---"); // Adding some separation
			System.out.println(t);
			break;
		default:
			System.out.println("Invalid choice");
		}
	}
	
	//views an object record with data of user choosing
		public static void viewItem(){
			
			System.out.println("What record do you want to view? (1-" + totalSlots + ")");
			
			int currentSlot = Integer.parseInt(userInput.nextLine()) - 1;
			
			// We will ask the user to select only one of the object types available
			System.out.println("Choose which type of object you would like to view.");
			System.out.println("1) Person");
			System.out.println("2) Staff");
			System.out.println("3) Faculty");
			System.out.println("4) Full-Time Faculty");
			System.out.println("5) Part-Time Faculty");
			System.out.println("6) Student");
			System.out.println("7) Tutor (Student)");
			
			System.out.print("Choice: ");
			int choiceObject = Integer.parseInt(userInput.nextLine());
			
			System.out.println();
			
			switch(choiceObject) {
			case 1: // Person
				p.setCurrentSlot(currentSlot);
				System.out.println("--- --- --- --- ---"); // Adding some separation
				System.out.println(p);
				break;
			case 2: // Staff
				s.setCurrentSlot(currentSlot);
				System.out.println("--- --- --- --- ---"); // Adding some separation
				System.out.println(s);
				break;
			case 3: // Faculty
				f.setCurrentSlot(currentSlot);
				System.out.println("--- --- --- --- ---"); // Adding some separation
				System.out.println(f);
				break;
			case 4: //Full-Time Faculty
				ft.setCurrentSlot(currentSlot);
				System.out.println("--- --- --- --- ---"); // Adding some separation
				System.out.println(ft);
				break;
			case 5: // Part-Time Faculty
				pt.setCurrentSlot(currentSlot);
				System.out.println("--- --- --- --- ---"); // Adding some separation
				System.out.println(pt);
				break;
			case 6: // Student
				st.setCurrentSlot(currentSlot);
				System.out.println("--- --- --- --- ---"); // Adding some separation
				System.out.println(st);
				break;
			case 7: // Tutor
				t.setCurrentSlot(currentSlot);
				System.out.println("--- --- --- --- ---"); // Adding some separation
				System.out.println(t);
				break;
			default:
				System.out.println("Invalid choice");
			}
			//userInput.close();
		}
		
	
	
	/**
	 * Method that collects information relevant to the Employee interface
	 * @param e An object that implements that Employee interface
	 * @param userInput A Scanner used to collect information fromt he user
	 */
	public static void getEmployeeInformation(Employee e, Scanner userInput) {
		do {
			System.out.print("Enter the employment start date (YYYY/MM/DD): ");
		} while (!e.setStartDate(userInput.nextLine()));
		
		do {
			System.out.print("Enter the salary: ");
		} while (!e.setSalary(Integer.parseInt(userInput.nextLine())));
	}
	
	/**
	 * Method that collects information relevant to objects of type Staff
	 * @param s An object of type Staff
	 * @param userInput A Scanner used to collect information from the user
	 */
	public static void getStaffInfo(Staff s, Scanner userInput) {
		// Since Staff inherits from Person, we can utilize the method
		// that collects information about a Person first, then we
		// will collect information related to the Staff class
		getPersonInfo(s, userInput);
		
		// Now we can collect information that belongs solely to Staff
		
		// Department
		do {
			System.out.print("Please enter the Department's name (3-50 characters): ");
		} while(!s.setDepartment(userInput.nextLine()));
		
		// Supervisor
		do {
			System.out.print("Please enter the Supervisor's name (2-50 characters): ");
		} while(!s.setSupervisor(userInput.nextLine()));
		
		getEmployeeInformation(s, userInput);
	}
	
	/**
	 * Method that collects information relevant to objects of type Faculty
	 * @param f An object of type Faculty
	 * @param userInput A Scanner used to collect information from the user
	 */
	public static void getFacultyInfo(Faculty f, Scanner userInput) {
		getPersonInfo(f, userInput);
		
		// Division
		do {
			System.out.print("Please enter the Division's name (3-50 characters): ");
		} while(!f.setDivision(userInput.nextLine()));
		
		// Degree
		do {
			System.out.print("Please enter the highest degree earned (Bachelor's, Master's, Doctorate): ");
		} while(!f.setDegree(userInput.nextLine()));
		
		// Year of graduation
		do {
			System.out.print("Please enter the year of graduation: ");
		} while(!f.setYearOfGraduation(Integer.parseInt(userInput.nextLine())));
		
		getEmployeeInformation(f, userInput);
	}
	
	/**
	 * Method that collects information relevant to objects of type FullTime
	 * @param ft An object of type FullTime
	 * @param userInput A Scanner used to collect information from the user
	 */
	public static void getFullTimeFacultyInfo(FullTime ft, Scanner userInput) {
		getFacultyInfo(ft, userInput);
		
		// Rank
		do {
			System.out.print("Enter the rank (Lecturer, Assistant Professor, Associate Professor, Full Professor): ");
		} while(!ft.setRank(userInput.nextLine()));
	}
	
	/**
	 * Method that collects information relevant to objects of type PartTime
	 * @param pt An object of type PartTime
	 * @param userInput A Scanner used to collect information from the user
	 */
	public static void getPartTimeFacultyInfo(PartTime pt, Scanner userInput) {
		getFacultyInfo(pt, userInput);
		
		// Number of courses taught
		do {
			System.out.print("Please enter the number of courses taught (1-100): ");
		} while(!pt.setCoursesTaught(Integer.parseInt(userInput.nextLine())));
	}
	
	/**
	 * Method that collects information relevant to objects of type Student
	 * @param s An object of type Student
	 * @param userInput A Scanner used to collect information from the user
	 */
	public static void getStudentInfo(Student s, Scanner userInput) {
		getPersonInfo(s, userInput);
		
		//Class standing
		do {
			System.out.print("Please enter the class standing (Freshmen, Sophomore, Junior, Senior, Graduate): ");
		} while(!s.setClassStanding(userInput.nextLine()));
		
		//Permanent address
		do {
			System.out.print("Please enter the permanent address (10-300 characters): ");
		} while(!s.setPermanentAddress(userInput.nextLine()));
		
		//GPA
		do {
			System.out.print("Please enter the GPA (0.0-4.0): ");
		} while(!s.setGPA(Double.parseDouble(userInput.nextLine())));
	}
	
	/**
	 * Method that collects information relevant to objects of type Tutor
	 * @param t An object of type Tutor
	 * @param userInput A Scanner used to collect information from the user
	 */
	public static void getTutorInfo(Tutor t, Scanner userInput) {
		getStudentInfo(t, userInput);
		
		// Topics
		do {
			System.out.print("Enter the topics of expertise (separated by commas): ");
		} while(!t.setTopics(userInput.nextLine()));
		
		// Hours
		do {
			System.out.print("Enter the number of hours tutored so far: ");
		} while(!t.setHours(Integer.parseInt(userInput.nextLine())));
		
		getEmployeeInformation(t, userInput);
	}
	
	/**
	 * Method that collects information relevant to objects of type Person 
	 * @param p An object of type Person
	 * @param userInput A Scanner used to collect information from the user
	 */
	public static void getPersonInfo(Person p, Scanner userInput) {
		do { // First name
			System.out.print("Please enter the first name (2-50 characters): ");
		} while(!p.setFirstName(userInput.nextLine()));
		
		do { // Last name
			System.out.print("Please enter the last name (2-50 characters): ");
		} while(!p.setLastName(userInput.nextLine()));
		
		do { // ID
			System.out.print("Please enter the ID (5 digits): ");
		} while(!p.setId(Integer.parseInt(userInput.nextLine())));
		
		do { // Birth date
			System.out.print("Please enter the birth date (YYYY/MM/DD): ");
		} while(!p.setBirthDate(userInput.nextLine()));
		
		do { // Address
			System.out.print("Please enter the address (10-300 characters): ");
		} while(!p.setAddress(userInput.nextLine()));
	}
//	}catch(Exception e) {}
}
