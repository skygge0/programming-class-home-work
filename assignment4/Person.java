package assignment4;

import java.time.LocalDate;
import java.time.Period;

/**
 * This class is for Assignment 4 for COSC 351.101, Fall 2018.
 * @author Giovanni Vincenti, Halvor Remole, Nicholas Shrieves
 * @since 2018-10-12
 */

public class Person {
	/**
	 * Main constructor that sets default values to the constants.
	 * @param currentSlot 
	 */
	
	int currentSlot = 0;
	
	public Person() {
		firstName[currentSlot] = NOT_AVAIL;
		lastName[currentSlot] = NOT_AVAIL;
		ID[currentSlot] = NO_ID;
		birthDate[currentSlot] = NO_BDATE;
		address[currentSlot] = NOT_AVAIL;
		//this.currentSlot = currentSlot;
	}
	
	private String[] firstName = new String[5];
	private String[] lastName = new String[5];
	int[] ID = new int[5];
	private String[] birthDate = new String[5];
	private String[] address = new String[5];
	
	// The following constants to have a reference to default values
	protected static final String NOT_AVAIL = "Not Available";
	protected static final int NO_ID = 99999;
	protected static final String NO_BDATE = "1800/01/01";
	
	/**
	 * Sets the currentSlot
	 */
	public void setCurrentSlot(int currentSlot) {
		this.currentSlot = currentSlot;
	}
	
	/**
	 * Returns the first name of the person.
	 * @return String containing the first name
	 */
	public String getFirstName() {
		return firstName[currentSlot];
	}
	
	/**
	 * Sets the first name of the person.
	 * @param firstName String containing the first name between 2 and 50 alphabetic characters, including apostrophes, spaces, and dashes
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setFirstName(String newFirstName) {
		// Null parameter
		if ( firstName == null )  return false;
		
		// Removing leading and trailing spaces
		newFirstName = newFirstName.trim();
		
		// Incorrect length
		if ( newFirstName.length() < 2 || newFirstName.length() > 50 ) return false;
		
		// Checking for the correct characters
		if ( !newFirstName.matches("[a-zA-Z\\'\\-\\ ]+") ) return false;
		
		firstName[currentSlot] = newFirstName;
		return true;
	}
	
	/**
	 * Returns the last name of the person.
	 * @return String containing the last name
	 */
	public String getLastName() {
		return lastName[currentSlot];
	}
	
	/**
	 * Sets the last name of the person.
	 * @param lastName String containing the last name between 2 and 50 alphabetic characters, including apostrophes, spaces, and dashes
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setLastName(String newLastName) {
		// Null parameter
		if ( lastName == null )  return false;
		
		// Removing leading and trailing spaces
		newLastName = newLastName.trim();
		
		// Incorrect length
		if ( newLastName.length() < 2 || newLastName.length() > 50 ) return false;
		
		// Checking for the correct characters
		if ( !newLastName.matches("[a-zA-Z\\'\\-\\ ]+") ) return false;
		
		lastName[currentSlot] = newLastName;
		return true;
	}
	
	/**
	 * Returns the ID of the person.
	 * @return int containing the ID number
	 */
	public int getId() {
		return ID[currentSlot];
	}
	
	/**
	 * Sets the ID of the person.
	 * @param id int containing the ID number between 10000 and 99999
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setId(int newID) {
		if ( newID < 10000 || newID > 99999 )
			return false;
		ID[currentSlot] = newID;
		return true;
	}
	
	/**
	 * Returns the birth date of the person.
	 * @return String containing the birth date
	 */
	public String getBirthDate() {
		return birthDate[currentSlot];
	}
	
	/**
	 * Sets the birth date of the person.
	 * @param birthDate String containing the birth date in the format YYYY/MM/DD
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setBirthDate(String newBirthDate) {
		if ( newBirthDate == null ) return false;
		
		newBirthDate = newBirthDate.trim();
		
		// This is only checking for the format, not for a valid date (ex: February 31 2019 is accepted)
		if ( !newBirthDate.matches("[0-9]{4}/[0-9]{2}/[0-9]{2}") ) return false;
		
		birthDate[currentSlot] = newBirthDate;
		return true;
	}
	
	/**
	 * Returns the address of the person.
	 * @return String containing the address
	 */
	public String getAddress() {
		return address[currentSlot];
	}
	
	/**
	 * Sets the address of the person.
	 * @param address String containing the address
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setAddress(String newAddress) {
		if ( address == null ) return false;
		
		newAddress = newAddress.trim();
		
		// Checking the length of the address
		if ( newAddress.length() < 10 || newAddress.length() > 300 ) return false;
		
		if ( !newAddress.matches("[a-zA-Z0-9,.\\'\\-\\ ]+") ) return false;
		
		address[currentSlot] = newAddress;
		return true;
	}
	
	/**
	 * Gets the email address of the person in standard firstname.lastname@ubalt.edu format.
	 * @return String containing the email address of the person
	 */
	public String getEmailAddress() {
		// If there is either no first or last name
		if ( firstName[currentSlot].equalsIgnoreCase(NOT_AVAIL) || lastName[currentSlot].equalsIgnoreCase(NOT_AVAIL) ) return NOT_AVAIL;
		
		// The first name may contain spaces, dashes, and apostrophes, which need to be removed
		String sanitizedFirstName = firstName[currentSlot].replaceAll("[\\-\\'\\ ]", "");
		// Email addresses usually utilize lower-case letters only
		sanitizedFirstName = sanitizedFirstName.toLowerCase();
		
		// The same modifications need to be applied to the last name
		String sanitizedLastName = lastName[currentSlot].replaceAll("[\\-\\'\\ ]", "");
		sanitizedLastName = sanitizedLastName.toLowerCase();
		
		String emailAddress = sanitizedFirstName + "." + sanitizedLastName;
		
		emailAddress += "@ubalt.edu";
		
		return emailAddress;
	}
	
	/**
	 * Gets the age of the person, based on the birth date and the current date.
	 * @return int containing the age of the person
	 */
	public int getAge() {
		// If no birth date has been set
		if ( birthDate[currentSlot].equalsIgnoreCase(NO_BDATE) ) return -1;
		
		// The following is just one way to solve this problem. You may
		// have found a different way, which is fine as well. Please use
		// the following code as an example.
		
		// A way to calculate the age is to use the birthday as a point in time,
		// create a Calendar object that accepts that format, and then compare
		// the birth date to today's date
		
		// The following is possible only because we accept the specific format
		// of YYYY/MM/DD. Alternatively, we can replace the / with - and it will
		// still work. If we should replace the number of characters associated
		// with each entry (ex: 1 instead of 01) then the system will not work.
		// Also, if we should swap the sequence of the date elements, it will not work.
		
		// Extracting the first four numbers, the year
		int birthYear = Integer.parseInt(birthDate[currentSlot].substring(0, 4));
		
		// Extracting the numbers associated with the month
		int birthMonth = Integer.parseInt(birthDate[currentSlot].substring(5, 7));
		
		// Extracting the numbers associated with the day
		int birthDay = Integer.parseInt(birthDate[currentSlot].substring(8));
		
		// Creates two LocalDate type objects, which we will use
		// to figure out the age of the record
		LocalDate bDate = LocalDate.of(birthYear, birthMonth, birthDay);
		LocalDate today = LocalDate.now();
		
		// We let the system figure out how many years are in between the two dates
		int age = Period.between(bDate, today).getYears();
		
		return age;
	}
	
	/**
	 * Method that returns in String format the information stored in the object.
	 * @return A string containing the information stored in the object
	 */
	public String toString() {
		// The following will let the system understand which newline character
		// should be used, since Windows and UNIX-based systems (Linux, Mac) have
		// different symbols representing the end of line
		String newline = System.getProperty("line.separator");
		
		// Building the string to return
		String toReturn = firstName[currentSlot] + " " + lastName[currentSlot] + " (ID: " + ID[currentSlot] + ", " + getEmailAddress() + ")" + newline;
		toReturn += "Birth date: " + birthDate[currentSlot] + " (age: " + getAge() + ")" + newline;
		toReturn += "Address: " + address[currentSlot] + newline;
		return toReturn;
	}
}
