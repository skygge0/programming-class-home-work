package assignment4;

import java.util.Calendar;

/**
 * This class is for Assignment 4 for COSC 351.101, Fall 2018.
 * @author Giovanni Vincenti, Halvor Remole, Nicholas Shrieves
 * @since 2018-10-12
 */

public class Faculty extends Person implements Employee {
	
	int currentSlot = 0;
	
	/**
	 * Constructor that sets the default values.
	 */
	public Faculty() {
		super();
		division[currentSlot] = Person.NOT_AVAIL;
		degree[currentSlot] = Person.NOT_AVAIL;
		yearOfGraduation[currentSlot] = -1;

		salary[currentSlot] = Employee.DEFAULT_SALARY;
		startDate[currentSlot] = Employee.DEFAULT_START_DATE;
		
		//this.currentSlot = currentSlot;
	}
	
	private String[] division = new String[5];
	private String[] degree = new String[5];
	private int[] yearOfGraduation = new int[5];
	
	private final String[] listOfDegrees = {"Bachelor's", "Master's", "Doctorate"};
	
	/**
	 * Sets the currentSlot
	 */
	public void setCurrentSlot(int currentSlot) {
		this.currentSlot = currentSlot;
	}
	
	/**
	 * Gets the Division where the faculty member works.
	 * @return String containing the name of the Division
	 */
	public String getDivision() {
		return division[currentSlot];
	}
	
	/**
	 * Sets the name of the Division in which the faculty member works.
	 * @param division String containing the name of the Division
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setDivision(String division) {
		if ( division == null ) return false;
		
		division = division.trim();
		
		// Incorrect length
		if ( division.length() < 3 || division.length() > 50 ) return false;
		
		// Checking for the correct characters
		if ( !division.matches("[a-zA-Z\\'\\-\\ ]+") ) return false;
		
		this.division[currentSlot] = division;
		return true;
	}
	
	/**
	 * Gets the highest level of the degree obtained by the faculty member
	 * @return String containing the highest lever of the degree obtained by the faculty member
	 */
	public String getDegree() {
		return degree[currentSlot];
	}
	
	/**
	 * Sets the highest level of the degree obtained by the faculty member (Bachelor's, Master's, Doctorate)
	 * @param newDegree String containing the highest level of the degree obtained by the faculty member
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setDegree(String newDegree) {
		if ( newDegree == null ) return false;
		
		newDegree = newDegree.trim();
		
		// Checking if the degree is among acceptable ones
		for ( String d : listOfDegrees ) { // Example of a 'for each' loop
			if ( d.equalsIgnoreCase(newDegree) ) {
				degree[currentSlot] = d; // We set the degree type from the array to make sure the casing is correct
				return true;
			}
		}

		// If we reach the end of the loop without any matches
		// that means that we cannot set the degree
		return false;
	}
	
	/**
	 * Gets the year of graduation of the faculty member.
	 * @return int containing the year of graduation of the faculty member
	 */
	public int getYearOfGraduation() {
		return yearOfGraduation[currentSlot];
	}
	
	/**
	 * Sets the year of graduation of the faculty member.
	 * @param yearOfGraduation int containing the year of graduation of the faculty member
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setYearOfGraduation(int yearOfGraduation) {
		if ( yearOfGraduation < 1950 || yearOfGraduation > Calendar.getInstance().get(Calendar.YEAR) )
			return false;
		
		this.yearOfGraduation[currentSlot] = yearOfGraduation;
		return true;
	}
	
	/**
	 * Gets the email address of the faculty member in the appropriate format.
	 * return String containing the email address of the faculty member
	 */
	public String getEmailAddress() { // Staff and Faculty have a different way to create the email address
		// If there is either no first or last name
		if ( getFirstName().equalsIgnoreCase(NOT_AVAIL) || getLastName().equalsIgnoreCase(NOT_AVAIL) ) return NOT_AVAIL;
		
		// The first name may contain spaces, dashes, and apostrophes, which need to be removed
		String sanitizedFirstName = getFirstName().replaceAll("[\\-\\'\\ ]", "");
		// Email addresses usually utilize lower-case letters only
		sanitizedFirstName = sanitizedFirstName.toLowerCase();
		
		// The same modifications need to be applied to the last name
		String sanitizedLastName = getLastName().replaceAll("[\\-\\'\\ ]", "");
		sanitizedLastName = sanitizedLastName.toLowerCase();
		
		String emailAddress = sanitizedFirstName.charAt(0) + sanitizedLastName;
		
		emailAddress += "@ubalt.edu";
		
		return emailAddress;
	}
	
	/**
	 * Method that returns in String format the information stored in the object.
	 * @return A string containing the information stored in the object
	 */
	public String toString() {
		String newline = System.getProperty("line.separator");
		String toReturn = super.toString();
		toReturn += "Employment start date: " + startDate + newline;
		toReturn += "Salary: $" + salary + newline;
		toReturn += "Division: " + getDivision() + newline;
		toReturn += "Degree: " + getDegree() + newline;
		toReturn += "Year of Graduation: " + getYearOfGraduation() + newline;
		return toReturn;
	}
	
	private int salary[] = new int[5];
	private String startDate[] = new String[5];

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSalary() {
		return salary[currentSlot];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setSalary(int salary) {
		if ( salary < 0 )
			return false;
		this.salary[currentSlot] = salary;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStartDate() {
		return startDate[currentSlot];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setStartDate(String startDate) {
		if ( startDate == null ) return false;
		
		if ( !startDate.matches("[0-9]{4}/[0-9]{2}/[0-9]{2}") ) return false;
		
		this.startDate[currentSlot] = startDate;
		return true;
	}
}