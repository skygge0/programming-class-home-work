package assignment4;

/**
 * This class is for Assignment 4 for COSC 351.101, Fall 2018.
 * @author Giovanni Vincenti, Halvor Remole, Nicholas Shrieves
 * @since 2018-10-12
 */

public interface Employee {
	
	public int DEFAULT_SALARY = 0;
	public String DEFAULT_START_DATE = "1900-01-01";
	
	/**
	 * A method used to get the salary of the employee
	 * @return An int containing the salary of the employee
	 */
	public int getSalary();
	
	/**
	 * A method used to set the salary of the employee
	 * @param salary An int with the employee's salary
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setSalary(int salary);
	
	/**
	 * A method used to get the start date of the employee
	 * @return A String in the format YYYY/MM/DD
	 */
	public String getStartDate();
	
	/**
	 * A method used to set the start date of employment
	 * @param startDate A String in the format YYYY/MM/DD
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setStartDate(String startDate);
}
