package assignment4;

/**
 * This class is for Assignment 4 for COSC 351.101, Fall 2018.
 * @author Giovanni Vincenti, Halvor Remole, Nicholas Shrieves
 * @since 2018-10-12
 */

public class FullTime extends Faculty {
	/**
	 * Constructor that sets the default values.
	 * @param currentSlot 
	 */
	int currentSlot = 0;
	
	public FullTime() {
		super();
		rank[currentSlot] = Person.NOT_AVAIL;
		
		//this.currentSlot = currentSlot;

	}

	private String rank[] = new String[5];
	
	// List of possible values for the rank
	private final String[] listofRanks = {"Lecturer", "Assistant Professor", "Associate Professor", "Full Professor"};
	
	/**
	 * Sets the currentSlot
	 */
	public void setCurrentSlot(int currentSlot) {
		this.currentSlot = currentSlot;
	}

	/**
	 * Gets the rank of the faculty member.
	 * @return String containing the rank of the faculty member
	 */
	public String getRank() {
		return rank[currentSlot];
	}

	/**
	 * Sets the rank of the faculty member (Lecturer, Assistant Professor, Associate Professor, Full Professor).
	 * @param rank String containing the rank of the faculty member
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setRank(String rank) {
		if ( rank == null )
			return false;
		
		rank = rank.trim();
		
		for ( String r : listofRanks ) {
			if ( r.equalsIgnoreCase(rank) ) {
				// If there is a match, I will store the rank that is
				// contained in the array. In this way I do not have to
				// worry about the casing of the user's input.
				this.rank[currentSlot] = r;
				return true;
			}
		}

		return false;
	}
	
	/**
	 * Method that returns in String format the information stored in the object.
	 * @return A string containing the information stored in the object
	 */
	public String toString() {
		String newline = System.getProperty("line.separator");
		String toReturn = super.toString();
		toReturn += "Rank: " + getRank() + newline;
		return toReturn;
	}
}