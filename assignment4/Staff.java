package assignment4;

/**
 * This class is for Assignment 4 for COSC 351.101, Fall 2018.
 * @author Giovanni Vincenti, Halvor Remole, Nicholas Shrieves
 * @since 2018-10-12
 */

public class Staff extends Person implements Employee {
	int currentSlot = 0;
	
	/**
	 * Constructor that sets the default values.
	 * @param currentSlot 
	 */
	public Staff() {
		super();
		department[currentSlot] = Person.NOT_AVAIL;
		supervisor[currentSlot] = Person.NOT_AVAIL;
		
		salary[currentSlot] = Employee.DEFAULT_SALARY;
		startDate[currentSlot] = Employee.DEFAULT_START_DATE;
		
		//this.currentSlot = currentSlot;
	}
	private String[] department = new String[5];
	private String[] supervisor = new String[5];
	
	/**
	 * Sets the currentSlot
	 */
	public void setCurrentSlot(int currentSlot) {
		this.currentSlot = currentSlot;
	}
	
	/**
	 * Gets the Department where the staff member works
	 * @return String containing the name of the Department
	 */
	public String getDepartment() {
		return department[currentSlot];
	}
	
	/**
	 * Sets the name of the Department where the staff member works
	 * @param department String containing the name of the department
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setDepartment(String newDepartment) {
		if ( department == null )
			return false;
		
		newDepartment = newDepartment.trim();// Incorrect length
		
		if ( newDepartment.length() < 3 || newDepartment.length() > 50 ) return false;
		
		// Checking for the correct characters
		if ( !newDepartment.matches("[a-zA-Z\\'\\-\\ ]+") ) return false;
		
		department[currentSlot] = newDepartment;
		return true;
	}
	
	/**
	 * Gets the name of the staff member's supervisor.
	 * @return String containing the name of the supervisor
	 */
	public String getSupervisor() {
		return supervisor[currentSlot];
	}
	
	/**
	 * Sets the name of the staff member's supervisor
	 * @param newSupervisor String containing the name of the supervisor
	 * @return Boolean reporting whether or not the attribute was successfully updated
	 */
	public boolean setSupervisor(String newSupervisor) {
		if ( newSupervisor == null )
			return false;
		
		newSupervisor = newSupervisor.trim();// Incorrect length
		
		if ( newSupervisor.length() < 2 || newSupervisor.length() > 50 ) return false;
		
		// Checking for the correct characters
		if ( !newSupervisor.matches("[a-zA-Z\\'\\-\\ ]+") ) return false;
		
		supervisor[currentSlot] = newSupervisor;
		return true;
	}
	
	/**
	 * Gets the email address of the staff member in the appropriate format.
	 * return String containing the email address of the staff member
	 */
	public String getEmailAddress() { // Staff and Faculty have a different way to create the email address
		// If there is either no first or last name
		if ( getFirstName().equalsIgnoreCase(NOT_AVAIL) || getLastName().equalsIgnoreCase(NOT_AVAIL) ) return NOT_AVAIL;
		
		// The first name may contain spaces, dashes, and apostrophes, which need to be removed
		String sanitizedFirstName = getFirstName().replaceAll("[\\-\\'\\ ]", "");
		// Email addresses usually utilize lower-case letters only
		sanitizedFirstName = sanitizedFirstName.toLowerCase();
		
		// The same modifications need to be applied to the last name
		String sanitizedLastName = getLastName().replaceAll("[\\-\\'\\ ]", "");
		sanitizedLastName = sanitizedLastName.toLowerCase();
		
		String emailAddress = sanitizedFirstName.charAt(0) + sanitizedLastName;
		
		emailAddress += "@ubalt.edu";
		
		return emailAddress;
	}
	
	/**
	 * Method that returns in String format the information stored in the object.
	 * @return A string containing the information stored in the object
	 */
	public String toString() {
		String newline = System.getProperty("line.separator");
		
		String toReturn = super.toString();
		toReturn += "Employment start date: " + startDate[currentSlot] + newline;
		toReturn += "Salary: $" + salary[currentSlot] + newline;
		toReturn += "Department: " + getDepartment() + newline;
		toReturn += "Supervisor: " + getSupervisor() + newline;
		return toReturn;
	}
	
	private int[] salary = new int[5];
	private String[] startDate = new String[5];

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSalary() {
		return salary[currentSlot];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setSalary(int newSalary) {
		if ( newSalary < 0 )
			return false;
		salary[currentSlot] = newSalary;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStartDate() {
		return startDate[currentSlot];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setStartDate(String newStartDate) {
		if ( newStartDate == null ) return false;
		
		if ( !newStartDate.matches("[0-9]{4}/[0-9]{2}/[0-9]{2}") ) return false;
		
		startDate[currentSlot] = newStartDate;
		return true;
	}
}
