package assignment1;

import java.util.Scanner;

/**
 * This class was created for assignment 1
 * @author Halvor Remole
 */

public class Assignment1 {
	/**
	 * This is the main function for this program
	 * @param args Not in use for this application
	 */
	public static void main(String[] args) {
		
		Scanner userInput = new Scanner(System.in);
		Person p = new Person();
		
		/**
		 * Data input
		 */
		System.out.print("Please enter your first name: ");
		p.setFirstName(userInput.nextLine());
		
		System.out.print("Please enter your last name: ");
		p.setLastName(userInput.nextLine());
		
		System.out.print("Please enter your identification: ");
		p.setID(Integer.parseInt(userInput.nextLine()));
		
		System.out.print("Please enter your birthday: ");
		p.setBirthday(userInput.nextLine());
		
		System.out.print("Please enter your address: ");
		p.setAddress(userInput.nextLine());
		
		/**
		 * Data output
		 */
		System.out.printf("%16s%-8s%n","First Name: ", p.getFirstName());
		System.out.printf("%16s%-8s%n", " Last Name: ", p.getLastName());
		System.out.printf("%16s%-8d%n", "Identification: ", p.getID());
		System.out.printf("%16s%-8s%n", "Birthday: ", p.getBirthday());
		System.out.printf("%16s%-8s%n", "Address: ", p.getAddress());

		
		//closes scanner
		userInput.close();
	}

}

/*
The main function should create an instance of the object you have created, then ask the user for inputs, and
store the information in the object. The main function will then output all the information contained in the
object. Use the code created for lectures as a guide
*/