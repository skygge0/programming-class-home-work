package assignment1;
/**
 * This class was created for assignment 1
 * @author Halvor Remole
 */
public class Person {
	
	/**
	 * Person default constructor
	 */
	public Person() {
		firstName = "N/A";
		lastName = "N/A";
		ID = -1;
		birthday = "N/A";
		address = "N/A";
	}
	
	/**
	 * Person Variables
	 */
	private String firstName;
	private String lastName;
	private int ID;
	private String birthday;
	private String address;

	/*
	 * Person get and set methods
	 */
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}
	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}
	/**
	 * @return the birthday
	 */
	public String getBirthday() {
		return birthday;
	}
	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}	
}
