package assignment2;
/**
 * This class was created for Assignment 2
 * @author Thomas Mejia, Halvor Remole
 * @date 2018-10-08
 */
public class FullTime extends Faculty{
	
	/**
	 * FullTime default constructor
	 */
	public FullTime() {
		rank = "N/A";
	}

	/**
	 * FullTime variables
	 */
	private String rank;

	
	/**
	 * @return the rank
	 */
	public String getRank() {
		return rank;
	}

	/**
	 * @param rank the rank to set
	 */
	public boolean setRank(String rank) {
		if(null != rank && rank.matches("^(Lecturer|Assistant Professor|Associate Professor|Full Professor)")) {
			this.rank = rank;
			return true;
		}
		return false;
	}
}
