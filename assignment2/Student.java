package assignment2;
/**
 * This class was created for Assignment 2
 * @author Thomas Mejia, Halvor Remole
 * @date 2018-10-08
 */
public class Student extends Person{
	
	/**
	 * Student default constructor
	 */
	public Student() {
		classStanding = "N/A";
		GPA = -1;
	}

	/**
	 * Student variables
	 */
	private String classStanding;
	private String permAddress;
	private double GPA;
	
	
	/**
	 * @return the classStanding
	 */
	public String getClassStanding() {
		return classStanding;
	}
	/**
	 * @param classStanding the classStanding to set
	 */
	public boolean setClassStanding(String classStanding) {
		if(null != classStanding &&  classStanding.matches("^(Freshmen|Sophomore|Junior|Senior|Graduate)")) {
			this.classStanding = classStanding;
			return true;
		}
		return false;
	}
	/**
	 * @return the permanentAddress
	 */
	public String getPermanentAddress() {
		return getAddress();
	}
	/**
	 * @param permanentAddress the permanentAddress to set
	 */
	public void setPermanentAddress(String permanentAddress) {
		setAddress(permanentAddress);
	}
	/**
	 * @return the gPA
	 */
	public double getGPA() {
		return GPA;
	}
	/**
	 * @param GPA the GPA to set
	 */
	public boolean setGPA(double GPA) {
		if(GPA >= 0 && GPA <= 4) {
			this.GPA = GPA;
			return true;
		}
		return false;
	}
}
