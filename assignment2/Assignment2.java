package assignment2;

import java.util.Scanner;

/**
 * This class was created for Assignment 2
 * @author Thomas Mejia, Halvor Remole
 * @date 2018-10-08
 */

public class Assignment2 {
	/**
	 * This is the main function for this program
	 * @param args Not in use for this application
	 */
	public static void main(String[] args) {
		
		/**
		 * Calls constructors and creates object instances of each class
		 */
		Scanner userInput = new Scanner(System.in);
		Person per = new Person();
		Staff sta = new Staff();
		Faculty fac = new Faculty();
		FullTime ful = new FullTime();
		PartTime par = new PartTime();
		Student stu = new Student();
		Tutor tut = new Tutor();

		/**
		 * default menu settings
		 */
		boolean student = false;
		boolean tutor = false;
		boolean staff = false;
		boolean faculty = false;
		boolean fullTime = false;
		
		/**
		 * Strings to be printed for display
		 */
		String printLine = "";
		String firstName = "";
		String lastName = "";
		
		//Data input
		/**
		 * Menu first name input
		 */
		System.out.print("Please enter your first name: ");
		firstName = userInput.nextLine();
		/**
		 * Menu last name input
		 */
		System.out.print("Please enter your last name: ");
		lastName = userInput.nextLine();
		/**
		 * Menu identification input
		 */
		System.out.print("Please enter your identification: ");
		per.setID(Integer.parseInt(userInput.nextLine()));
		printLine += "                ID: " + per.getID() + "\n";
		/**
		 * Menu birthday input
		 */
		System.out.print("Please enter your birthday: ");
		per.setBirthday(userInput.nextLine());
		printLine += "          Birthday: " + per.getBirthday() + "\n";
		/**
		 * Menu address input
		 */
		System.out.print("Please enter your address: ");
		per.setAddress(userInput.nextLine());
		printLine += "           Address: " + per.getAddress() + "\n";
		
		//Conditional data input
		/**
		 * Menu student decision
		 */
		System.out.print("Are you a student?: ");
		if( "yes".equals(userInput.nextLine()) ) {
			student = true;
		}
		/**
		 * Menu student fork
		 */
		if(true == student ) {	//questions for just students
			/**
			 * Menu student name setter call
			 */
			per.setFirstName(firstName); //saves name to right locations
			per.setLastName(lastName);
			/**
			 * Menu student standing input
			 */
			System.out.print("Please enter your class standing (Freshmen, Sophomore, Junior, Senior, Graduate): ");
			stu.setClassStanding(userInput.nextLine());		//stores value
			printLine += "             Class: " + stu.getClassStanding() + "\n";		//starts a string using value to output later
			/**
			 * Menu student GPA input
			 */
			System.out.print("Please enter your GPA: ");
			stu.setGPA(Double.parseDouble(userInput.nextLine()));
			printLine += "               GPA: " + stu.getGPA() + "\n";
			/**
			 * Menu student permanent address input
			 */
			System.out.print("Please enter your permanent address: ");
			stu.setPermanentAddress((userInput.nextLine()));
			printLine += " Permanent Address: " + stu.getPermanentAddress() + "\n";
			
			/**
			 * Menu tutor decision
			 */
			System.out.print("Are you a tutor?: ");
			if( "yes".equals(userInput.nextLine()) ) {
				tutor = true;
			}
			/**
			 * Menu tutor fork
			 */
			if( true == tutor ) {	//questions for just tutors
				/**
				 * Menu tutor topic input
				 */
				System.out.print("Please enter your topic: ");
				tut.setTopics(userInput.nextLine());		
				printLine += "             Topic: " + tut.getTopics() + "\n";
				/**
				 * Menu tutor hours input
				 */
				System.out.print("Please enter your hours taught: ");
				tut.setHours(Integer.parseInt(userInput.nextLine()));
				printLine += "      Hours taught: " + tut.getHours() + "\n";
				/**
				 * Menu tutor salary input
				 */
				System.out.print("Please enter your salary: ");
				tut.setSalary(Double.parseDouble(userInput.nextLine()));
				printLine += "            Salary: " + tut.getSalary() + "\n";
				/**
				 * Menu tutor start date input
				 */
				System.out.print("Please enter your start date: ");
				tut.setStartDate(userInput.nextLine());
				printLine += "        Start Date: " + tut.getStartDate() + "\n";
			}
		/**
		 * Menu staff fork
		 */
		}else {	//must be staff so staff only inputs, etc.
			staff = true;
			/**
			 * Menu staff name setter call
			 */			
			sta.setFirstName(firstName);
			sta.setLastName(lastName);
			/**
			 * Menu staff department input
			 */
			System.out.print("Please enter your department: ");
			sta.setDepartment(userInput.nextLine());
			printLine += "        Department: " + sta.getDepartment() + "\n";
			/**
			 * Menu staff supervisor input
			 */
			System.out.print("Please enter your supervisor: ");
			sta.setSupervisor(userInput.nextLine());
			printLine += "        Supervisor: " + sta.getSupervisor() + "\n";
			/**
			 * Menu staff salary input
			 */
			System.out.print("Please enter your salary: ");
			sta.setSalary(Double.parseDouble(userInput.nextLine()));
			printLine += "            Salary: " + sta.getSalary() + "\n";
			/**
			 * Menu staff start date input
			 */
			System.out.print("Please enter your start date: ");
			sta.setStartDate(userInput.nextLine());
			printLine += "        Start Date: " + sta.getStartDate() + "\n";
			
			/**
			 * Menu faculty decision
			 */
			System.out.print("Are you faculty?: ");
			if( "yes".equals(userInput.nextLine()) ) {
				faculty = true;
			}
			/**
			 * Menu faculty fork
			 */
			if( true == faculty ) {
				/**
				 * Menu faculty name setter call
				 */	
				fac.setFirstName(firstName);
				fac.setLastName(lastName);
				/**
				 * Menu faculty division input
				 */
				System.out.print("Please enter your division: ");
				fac.setDivision(userInput.nextLine());
				printLine += "          Division: " + fac.getDivision() + "\n";
				/**
				 * Menu faculty degree input
				 */
				System.out.print("Please enter your degree (Bachelor's, Master's, Doctorate): ");
				fac.setDegree(userInput.nextLine());
				printLine += "            Degree: " + fac.getDegree() + "\n";
				/**
				 * Menu faculty graduation year input
				 */
				System.out.print("Please enter your graduation year: ");
				fac.setGradYear(Integer.parseInt(userInput.nextLine()));
				printLine += "   Graduation Year: " + fac.getGradYear() + "\n";
				/**
				 * Menu faculty salary input
				 */
				System.out.print("Please enter your salary: ");
				fac.setSalary(Double.parseDouble(userInput.nextLine()));
				printLine += "            Salary: " + fac.getSalary() + "\n";
				/**
				 * Menu faculty start date input
				 */
				System.out.print("Please enter your start date: ");
				fac.setStartDate(userInput.nextLine());
				printLine += "        Start Date: " + fac.getStartDate() + "\n";
				
				/**
				 * Menu full time decision
				 */
				System.out.print("Are you full time?: ");
				if( "yes".equals(userInput.nextLine()) ) {
					fullTime = true;
				}
				/**
				 * Menu full time fork
				 */
				if( true == fullTime ) {
					/**
					 * Menu full time rank input
					 */
					System.out.print("Please enter your rank (Lecturer, Assistant Professor, Associate Professor, Full Professor): ");
					ful.setRank(userInput.nextLine());
					printLine += "              Rank: " + ful.getRank() + "\n";
				/**
				 * Menu part time fork
				 */
				}else { //must be part time
					/**
					 * Menu part time number of courses input
					 */
					System.out.print("Please enter the number of courses taught: ");
					par.setCoursesTaught(Integer.parseInt(userInput.nextLine()));
					printLine += "     Course Taught: " + par.getCoursesTaught() + "\n";
				}
			}
		
		}
		
		/**
		 * Proper name and email output
		 */
		if( true == student ) {
			System.out.printf("%20s%-8s%n","First Name: ", per.getFirstName());
			System.out.printf("%20s%-8s%n", "Last Name: ", per.getLastName());
			System.out.printf("%20s%-8s%n", "Email: ", per.getEmail());
		}else if( true == staff ) {
			System.out.printf("%20s%-8s%n","First Name: ", sta.getFirstName());
			System.out.printf("%20s%-8s%n", "Last Name: ", sta.getLastName());
			System.out.printf("%20s%-8s%n", "Email: ", sta.getEmail());
		}else {
			System.out.printf("%20s%-8s%n","First Name: ", fac.getFirstName());
			System.out.printf("%20s%-8s%n", "Last Name: ", fac.getLastName());
			System.out.printf("%20s%-8s%n", "Email: ", fac.getEmail());
		}
	
		/**
		 * Primary text output
		 */
		System.out.println(printLine);
		
		//closes scanner
		userInput.close();
	}
}