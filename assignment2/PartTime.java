package assignment2;
/**
 * This class was created for Assignment 2
 * @author Thomas Mejia, Halvor Remole
 * @date 2018-10-08
 */
public class PartTime extends Faculty{
	
	/**
	 * PartTime default constructor
	 */
	public PartTime() {
		coursesTaught = 0;
	}

	/**
	 * PartTime variables
	 */
	private int coursesTaught;

	
	/**
	 * @return the coursesTaught
	 */
	public int getCoursesTaught() {
		return coursesTaught;
	}

	/**
	 * @param coursesTaught the coursesTaught to set
	 */
	public boolean setCoursesTaught(int coursesTaught) {
		if(coursesTaught >= 1 && coursesTaught <= 100) {
			this.coursesTaught = coursesTaught;
			return true;
		}
		return false;
	}
}
