package assignment2;
/**
 * This class was created for Assignment 2
 * @author Thomas Mejia, Halvor Remole
 * @date 2018-10-08
 */

interface Employee{
	
	double salary = 0;
	
	public boolean setSalary(double newSalary);
	
	public double getSalary();
	
	public boolean setStartDate(String newDate);
	
	public String getStartDate();
	
}

public class Person {
	
	/**
	 * Person default constructor
	 */
	public Person() {
		firstName = "N/A";
		lastName = "N/A";
		ID = -1;
		birthday = "1900/01/01";
		address = "N/A";
		email = "N/A";
		age = -1;
	}
	
	/**
	 * Person Variables
	 */
	private String firstName;
	private String lastName;
	private int ID;
	private String birthday;
	private String address;
	private String email;
	private int age;
	
	

	/*
	 * Person get and set methods
	 */
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public boolean setFirstName(String firstName) {
		if(null != firstName && firstName.matches("^[a-zA-Z-'\\s]{2,50}")) {
			this.firstName = firstName;
			return true;
		}
		return false;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public boolean setLastName(String lastName) {
		if(null != lastName && lastName.matches("^[a-zA-Z-'\\s]{2,50}")) {
			this.lastName = lastName;
			return true;
		}
		return false;
	}
	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}
	/**
	 * @param ID the ID to set
	 */
	public boolean setID(int ID) {
		if(5 == String.valueOf(ID).length()) {
			this.ID = ID;
			return true;
		}
		return false;
	}
	/**
	 * @return the birthday
	 */
	public String getBirthday() {
		return birthday;
	}
	/**
	 * @param birthday the birthday to set
	 */
	public boolean setBirthday(String birthday) {
		if(null != birthday && birthday.matches("^[0-9]{4}\\/[0-9]{2}\\/[0-9]{2}")) {
			this.birthday = birthday;
			return true;
		}
		return false;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public boolean setAddress(String address) {
		if(null != address && address.matches("^[a-zA-Z0-9-',.\\s]{10,300}")) {
			this.address = address;
			return true;
		}
		return false;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return firstName.toLowerCase().replaceAll("\\s", "") + "." + lastName.toLowerCase().replaceAll("\\s", "") + "@ubalt.edu";
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return 2018 - Integer.valueOf(birthday.substring(0, 4));
	}	
}
