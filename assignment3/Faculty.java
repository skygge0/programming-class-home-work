package assignment3;
/**
 * This class was created for Assignment 3
 * @author Halvor Remole
 * @previouslyAuthor Thomas Mejia
 * @date 2018-10-22
 */



public class Faculty extends Person implements Employee{
	
	String firstName = "";
	String lastName = "";
	String startDate = "";
	double salary = 0;
	
	public boolean setSalary(double newSalary){
		if(newSalary < 0) {
			return false;
		}else {
			salary = newSalary;
			return true;
		}			
	}
	public double getSalary() {
		return salary;
	}
	public boolean setStartDate(String newDate) {
		if(null != newDate && newDate.matches("^[0-9]{4}\\/[0-9]{2}\\/[0-9]{2}")) {
			startDate = newDate;
			return true;
		}
		return false;
	}
	
	public String getStartDate() {
		return startDate;
	}
	
	/**
	 * Faculty default constructor
	 */
	public Faculty() {
	}
	
	/**
	 * Faculty variables
	 */
	private String division = "N/A";
	private String degree = "N/A";
	private int gradYear = -1;
	
	/**
	 * @return the division
	 */
	public String getDivision() {
		return division;
	}
	/**
	 * @param division the division to set
	 */
	public boolean setDivision(String division) {
		if(null != division && division.matches("^[a-zA-Z-'\\s]{3,50}")) {
			this.division = division;
			return true;
		}
		return false;
	}
	/**
	 * @return the degree
	 */
	public String getDegree() {
		return degree;
	}
	/**
	 * @param degree the degree to set
	 */
	public boolean setDegree(String degree) {
		if(null != degree && degree.matches("^(Bachelor's|Master's|Doctorate)")) {
			this.degree = degree;
			return true;
		}
		return false;
	}
	/**
	 * @return the gradYear
	 */
	public int getGradYear() {
		return gradYear;
	}
	/**
	 * @param gradYear the gradYear to set
	 */
	public boolean setGradYear(int gradYear) {
		if(gradYear >= 1950 && gradYear <= 2018) {
			this.gradYear = gradYear;
			return true;
		}
		return false;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return getFirstName().substring(0,1).toLowerCase() + getLastName().toLowerCase() + "@ubalt.edu";
	}
}
