package assignment3;

import static org.junit.Assert.*;
import java.util.Calendar;
import org.junit.Test;

// based on provided junit tests from assignment 2

public class Assignment3Tester {
	
	// Testing the staff start date ---------------------------------------------
	
	/*
	 * submits a correctly formated date
	 * we need to ensure a real correct date goes through
	 * this test should return the same data as entered
	 * because the data matches the regex requirements in the setter
	 */
	@Test
	public void testCorrectStaffStartDate() {
		String input = "2000/01/01";
		Staff sta = new Staff();
		sta.setStartDate(input);
		assertEquals(input, sta.getStartDate());
	}
	/*
	 * submits an incorrectly formated date
	 * we need to ensure a false date does not go through
	 * this test should not return the same data as entered
	 * because the regex specifically looks for /'s between the digits
	 */
	@Test
	public void testIncorrectFormatStaffStartDate() {
		String input = "2000-01-01";
		Staff sta = new Staff();
		sta.setStartDate(input);
		assertNotEquals(input, sta.getStartDate());
	}
	/*
	 * submits an null date
	 * we need to ensure an empty date does not go through
	 * this test should not return the same data as entered
	 * because before the regex we check for not null
	 */
	@Test
	public void testNullStaffStartDate() {
		String input = null;
		Staff sta = new Staff();
		sta.setStartDate(input);
		assertNotEquals(input, sta.getStartDate());
	}
	/*
	 * submits an incorrectly charactered date
	 * we need to ensure a wrong character does not go through
	 * this test should not return the same data as entered
	 * because the regex specifically looks for digits between the /'s
	 */
	@Test
	public void testExtraCharactersStaffStartDate() {
		String input = "200O-01-01";
		Staff sta = new Staff();
		sta.setStartDate(input);
		assertNotEquals(input, sta.getStartDate());
	}
	
	// Testing the faculty start date ---------------------------------------------
	
	/*
	 * submits a correctly formated date
	 * we need to ensure a real correct date goes through
	 * this test should return the same data as entered
	 * because the data matches the regex requirements in the setter
	 */
	@Test
	public void testCorrectFacultyStartDate() {
		String input = "2000/01/01";
		Faculty fac = new Faculty();
		fac.setStartDate(input);
		assertEquals(input, fac.getStartDate());
	}
	/*
	 * submits an incorrectly formated date
	 * we need to ensure a false date does not go through
	 * this test should not return the same data as entered
	 * because the regex specifically looks for /'s between the digits
	 */
	@Test
	public void testIncorrectFormatFacultyStartDate() {
		String input = "2000-01-01";
		Faculty fac = new Faculty();
		fac.setStartDate(input);
		assertNotEquals(input, fac.getStartDate());
	}
	/*
	 * submits an null date
	 * we need to ensure an empty date does not go through
	 * this test should not return the same data as entered
	 * because before the regex we check for not null
	 */
	@Test
	public void testNullFacultyStartDate() {
		String input = null;
		Faculty fac = new Faculty();
		fac.setStartDate(input);
		assertNotEquals(input, fac.getStartDate());
	}
	/*
	 * submits an incorrectly charactered date
	 * we need to ensure a wrong character does not go through
	 * this test should not return the same data as entered
	 * because the regex specifically looks for digits between the /'s
	 */
	@Test
	public void testExtraCharactersFacultyStartDate() {
		String input = "200O-01-01";
		Faculty fac = new Faculty();
		fac.setStartDate(input);
		assertNotEquals(input, fac.getStartDate());
	}
		
	// Testing the tutor start date ---------------------------------------------
		
	/*
	 * submits a correctly formated date
	 * we need to ensure a real correct date goes through
	 * this test should return the same data as entered
	 * because the data matches the regex requirements in the setter
	 */
	@Test
	public void testCorrectTutorStartDate() {
		String input = "2000/01/01";
		Tutor tut = new Tutor();
		tut.setStartDate(input);
		assertEquals(input, tut.getStartDate());
	}
	/*
	 * submits an incorrectly formated date
	 * we need to ensure a false date does not go through
	 * this test should not return the same data as entered
	 * because the regex specifically looks for /'s between the digits
	 */
	@Test
	public void testIncorrectFormatTutorStartDate() {
		String input = "2000-01-01";
		Tutor tut = new Tutor();
		tut.setStartDate(input);
		assertNotEquals(input, tut.getStartDate());
	}
	/*
	 * submits an null date
	 * we need to ensure an empty date does not go through
	 * this test should not return the same data as entered
	 * because before the regex we check for not null
	 */
	@Test
	public void testNullTutorStartDate() {
		String input = null;
		Tutor tut = new Tutor();
		tut.setStartDate(input);
		assertNotEquals(input, tut.getStartDate());
	}
	/*
	 * submits an incorrectly charactered date
	 * we need to ensure a wrong character does not go through
	 * this test should not return the same data as entered
	 * because the regex specifically looks for digits between the /'s
	 */
	@Test
	public void testExtraCharactersTutorStartDate() {
		String input = "200O-01-01";
		Tutor tut = new Tutor();
		tut.setStartDate(input);
		assertNotEquals(input, tut.getStartDate());
	}
	
	// Testing the staff salary ---------------------------------------------
	
	/*
	 * submits a correct salary
	 * we need to ensure a real salary goes through
	 * this test should return the same data as entered
	 * because the data matches the positive requirements in the setter
	 */
	@Test
	public void testCorrectStaffSalary() {
		double input = 3.1d;
		Staff sta = new Staff();
		sta.setSalary(input);
		assertEquals(input, sta.getSalary(), 0.01); // Includes a precision loss of +/-0.01
	}
	/*
	 * submits an incorrect negative salary
	 * we need to ensure a negative salary does not go through
	 * this test should not return the same data as entered
	 * because the data does not match the positive requirements in the setter
	 */
	@Test
	public void testStaffLowSalary() {
		double input = -0.5d;
		Staff sta = new Staff();
		sta.setSalary(input);
		assertNotEquals(input, sta.getSalary(), 0.01); // Includes a precision loss of +/-0.01
	}
	
	// Testing the faculty salary ---------------------------------------------
	
	/*
	 * submits a correct salary
	 * we need to ensure a real salary goes through
	 * this test should return the same data as entered
	 * because the data matches the positive requirements in the setter
	 */
	@Test
	public void testCorrectFacultySalary() {
		double input = 3.1d;
		Faculty fac = new Faculty();
		fac.setSalary(input);
		assertEquals(input, fac.getSalary(), 0.01); // Includes a precision loss of +/-0.01
	}
	/*
	 * submits an incorrect negative salary
	 * we need to ensure a negative salary does not go through
	 * this test should not return the same data as entered
	 * because the data does not match the positive requirements in the setter
	 */
	@Test
	public void testFacultyLowSalary() {
		double input = -0.5d;
		Faculty fac = new Faculty();
		fac.setSalary(input);
		assertNotEquals(input, fac.getSalary(), 0.01); // Includes a precision loss of +/-0.01
	}
	
	// Testing the tutor salary ---------------------------------------------
	
	/*
	 * submits a correct salary
	 * we need to ensure a real salary goes through
	 * this test should return the same data as entered
	 * because the data matches the positive requirements in the setter
	 */
	@Test
	public void testCorrectTutorSalary() {
		double input = 3.1d;
		Tutor tut = new Tutor();
		tut.setSalary(input);
		assertEquals(input, tut.getSalary(), 0.01); // Includes a precision loss of +/-0.01
	}
	/*
	 * submits an incorrect negative salary
	 * we need to ensure a negative salary does not go through
	 * this test should not return the same data as entered
	 * because the data does not match the positive requirements in the setter
	 */
	@Test
	public void testTutorLowSalary() {
		double input = -0.5d;
		Tutor tut = new Tutor();
		tut.setSalary(input);
		assertNotEquals(input, tut.getSalary(), 0.01); // Includes a precision loss of +/-0.01
	}
}