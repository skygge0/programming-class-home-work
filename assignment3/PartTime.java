package assignment3;
/**
 * This class was created for Assignment 3
 * @author Halvor Remole
 * @previouslyAuthor Thomas Mejia
 * @date 2018-10-22
 */
public class PartTime extends Faculty{
	
	/**
	 * PartTime default constructor
	 */
	public PartTime() {
		coursesTaught = 0;
	}

	/**
	 * PartTime variables
	 */
	private int coursesTaught;

	
	/**
	 * @return the coursesTaught
	 */
	public int getCoursesTaught() {
		return coursesTaught;
	}

	/**
	 * @param coursesTaught the coursesTaught to set
	 */
	public boolean setCoursesTaught(int coursesTaught) {
		if(coursesTaught >= 1 && coursesTaught <= 100) {
			this.coursesTaught = coursesTaught;
			return true;
		}
		return false;
	}
}
