package assignment3;
/**
 * This class was created for Assignment 3
 * @author Halvor Remole
 * @previouslyAuthor Thomas Mejia
 * @date 2018-10-22
 */


public class Staff extends Person implements Employee{

	String firstName = "";
	String lastName = "";
	String startDate = "";
	double salary = 0;
	
	public boolean setSalary(double newSalary){
		if(newSalary < 0) {
			return false;
		}else {
			salary = newSalary;
			return true;
		}			
	}
	public double getSalary() {
		return salary;
	}
	public boolean setStartDate(String newDate) {
		if(null != newDate && newDate.matches("^[0-9]{4}\\/[0-9]{2}\\/[0-9]{2}")) {
			startDate = newDate;
			return true;
		}
		return false;
	}
	
	public String getStartDate() {
		return startDate;
	}
	
	/**
	 * Staff default constructor
	 */
	public Staff() {
	}
		
	/**
	 * Staff variables
	 */
	private String department = "N/A";
	private String supervisor = "N/A";
	
	
	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}
	/**
	 * @param department the department to set
	 */
	public boolean setDepartment(String department) {
		if(null != department &&  department.matches("^[a-zA-Z-'\\s]{3,50}")) {
			this.department = department;
			return true;
		}
		return false;
	}
	/**
	 * @return the supervisor
	 */
	public String getSupervisor() {
		return supervisor;
	}
	/**
	 * @param supervisor the supervisor to set
	 */
	public boolean setSupervisor(String supervisor) {
		if(null != supervisor && supervisor.matches("^[a-zA-Z-'\\s]{2,50}")) {
			this.supervisor = supervisor;
			return true;
		}
		return false;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return getFirstName().substring(0,1).toLowerCase() + getLastName().toLowerCase() + "@ubalt.edu";
	}
}
