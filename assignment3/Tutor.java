package assignment3;
/**
 * This class was created for Assignment 3
 * @author Halvor Remole
 * @previouslyAuthor Thomas Mejia
 * @date 2018-10-22
 */
public class Tutor extends Student implements Employee{
	
	public String startDate = "";
	public double salary = 0;
	
	public boolean setSalary(double newSalary){
		if(newSalary < 0) {
			return false;
		}else {
			salary = newSalary;
			return true;
		}			
	}
	public double getSalary() {
		return salary;
	}
	public boolean setStartDate(String newDate) {
		if(null != newDate && newDate.matches("^[0-9]{4}\\/[0-9]{2}\\/[0-9]{2}")) {
			startDate = newDate;
			return true;
		}
		return false;
	}
	
	public String getStartDate() {
		return startDate;
	}
	
	/**
	 * Tutor default constructor
	 */
	public Tutor() {
		topics = "N/A";
		hours = 0;
	}

	/**
	 * Tutor variables
	 */
	private String topics;
	private int hours;
	
	
	/**
	 * @return the topics
	 */
	public String getTopics() {
		return topics;
	}
	/**
	 * @param topics the topics to set
	 */
	public boolean setTopics(String topics) {
		if(null != topics &&  topics.matches("^[a-zA-Z,\\s]+")) {
			this.topics = topics;
			return true;
		}
		return false;
	}
	/**
	 * @return the hours
	 */
	public int getHours() {
		return hours;
	}
	/**
	 * @param hours the hours to set
	 */
	public boolean setHours(int hours) {
		if(hours >= 0 && hours <= 1000) {
			this.hours = hours;
			return true;
		}
		return false;
	}
}
